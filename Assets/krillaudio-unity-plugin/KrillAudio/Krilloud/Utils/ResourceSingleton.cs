using System.IO;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace KrillAudio.Krilloud.Utils
{
	public abstract class ResourceSingleton<T> : ScriptableObject where T : ScriptableObject
	{
#if UNITY_EDITOR
		private static string AssetPath = Path.Combine("Assets", "Resources");
#endif

		private static T _instance;
		public static T Instance
		{
			get
			{
				if (ReferenceEquals(_instance, null))
				{
					_instance = Resources.Load<T>(typeof(T).Name);
#if UNITY_EDITOR
					if (_instance == null)
					{
						CreateAsset();
					}
#endif
					var inst = _instance as ResourceSingleton<T>;
					if (inst != null)
					{
						inst.OnInstanceLoaded();
					}
				}
				return _instance;
			}
		}

#if UNITY_EDITOR

		private static void CreateAsset()
		{
			// Create directory
			var fullPath = Path.Combine(Application.dataPath, "Resources");
			if (!Directory.Exists(fullPath))
			{
				Directory.CreateDirectory(fullPath);
			}

			// Create singleton
			_instance = ScriptableObject.CreateInstance<T>();

			string path = Path.Combine(AssetPath, typeof(T).Name + ".asset");
			path = AssetDatabase.GenerateUniqueAssetPath(path);
			AssetDatabase.CreateAsset(_instance, path);

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

#endif

		protected virtual void OnInstanceLoaded()
		{
		}
	}
}