using UnityEngine;

namespace KrillAudio.Krilloud.Examples
{
	[RequireComponent(typeof(KLAudioSource))]
	public class KLChannelTest : MonoBehaviour
	{
		public int channel = 0;

		[Range(0f, 1f)]
		public float volume = 1;

		private KLAudioSource m_asource;
		public KLAudioSource asource
		{
			get
			{
				if (m_asource == null) m_asource = GetComponent<KLAudioSource>();
				return m_asource;
			}
		}

		#region Unity Events

		private void OnEnable()
		{
			asource.SetChannelVolume(channel, volume);
		}

		private void OnValidate()
		{
			if (!Application.isPlaying || !enabled) return;

			asource.SetChannelVolume(channel, volume);
		}

		#endregion Unity Events
	}
}