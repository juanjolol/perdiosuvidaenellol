﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowBase : MonoBehaviour
{
    protected Transform windowParent;
    protected GameObject cursor;

    public virtual void OnEnable()
    {
        cursor = GameObject.FindGameObjectWithTag("Cursor");
    }

    public virtual void Start()
    {
        windowParent = transform.parent;
    }
}
