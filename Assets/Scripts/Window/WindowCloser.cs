﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowCloser : WindowBase
{
    public override void OnEnable()
    {
        base.OnEnable();
    }

    public override void Start()
    {
        base.Start();
    }

    private void OnMouseDown()
    {
        switch (windowParent.name)
        {
            case "Log1_text Window":
                GameManager.instance.AdvanceStory("log1");
                break;
            case "Log2_text Window":
                GameManager.instance.AdvanceStory("log2");
                break;
            case "Log3_text Window":
                GameManager.instance.AdvanceStory("log3");
                break;
            case "Log4_text Window":
                GameManager.instance.AdvanceStory("log4");
                break;
            case "Log5_text Window":
                GameManager.instance.AdvanceStory("log5");
                break;
        }

        windowParent.gameObject.SetActive(false);
    }
}
