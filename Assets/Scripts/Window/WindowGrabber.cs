﻿using UnityEngine;

public class WindowGrabber : WindowBase
{
    private bool isDragging;

    public override void OnEnable()
    {
        base.OnEnable();
    }

    public override void Start()
    {
        base.Start();
    }

    private void Update ()
    {
        if (isDragging)
        {
            windowParent.position = cursor.transform.position;
        }

        if (Input.GetMouseButtonUp(0))
            isDragging = false;
    }

    private void OnMouseOver ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDragging = true;
        }
    }
}