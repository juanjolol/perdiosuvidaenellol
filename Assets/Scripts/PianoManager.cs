﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KrillAudio.Krilloud;

public class PianoManager : MonoBehaviour
{
    GameObject[] keys = new GameObject[6];
    string[] neededKeys = new string[] { "testKey","1", "1", "2", "1", "4", "3", "1", "1", "2", "1", "5", "4"};
    int keystate = 0;
    private IEnumerator coroutine;

    KLAudioSource klSource;

    void Start()
    {
        for (int i = 0; i < 6 ; i++)
        {
            keys[i] = transform.GetChild(i + 1).gameObject;
        }

        klSource = GetComponent<KLAudioSource>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            foreach (GameObject i in keys)
            {
                if (i.GetComponent<Collider2D>().OverlapPoint(mousePosition))
                {
                    coroutine = KeyFeedBack(i.transform.GetChild(0).gameObject);

                    StartCoroutine(coroutine);

                    keystate++;
                    AddKeyToSong(i.name);
                }
            }
        }
    }

    void AddKeyToSong(string keyId)
    {
        if (keyId == neededKeys[keystate])
        {
            klSource.SetIntVar("selector", int.Parse(keyId));
            klSource.Play("Piano Keys");

            GameObject.FindGameObjectWithTag("GameManager");

            if (keystate == 12)
            {
                GameManager.instance.AdvanceStory("piano");
                transform.parent.gameObject.SetActive(false);
            }
        }
        else
        {
            keystate = 0;
            klSource.SetIntVar("selector", 2);
            klSource.Play("SFXs");
        }
    }

    private IEnumerator KeyFeedBack(GameObject mask)
    {
        mask.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        mask.SetActive(false);
    }
}
