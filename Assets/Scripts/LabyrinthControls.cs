﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabyrinthControls : MonoBehaviour
{
    private bool followCursor;
    private Camera myCam;

    private Vector3 mousePos;
    private float distance;
    public float force;

    void Start()
    {
        myCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            followCursor = true;

        if (Input.GetMouseButtonUp(0)) 
            followCursor = false;

        if (followCursor)
        {
            if (followCursor)
            {
                mousePos = myCam.ScreenToWorldPoint(Input.mousePosition);
                distance = Vector2.Distance(transform.position, mousePos);

                gameObject.GetComponent<Rigidbody2D>().velocity = (mousePos - transform.position).normalized * distance * force;
            }

            /*Debug.Log(Vector2.Distance(transform.position, myCam.ScreenToWorldPoint(Input.mousePosition)));
            Debug.Log(gameObject.GetComponent<Rigidbody2D>().velocity);

            Debug.Log(gameObject.GetComponent<Rigidbody2D>().velocity);*/
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.name == "GoalTrigger")
        {
            GameManager.instance.AdvanceStory("lab");
            transform.parent.transform.parent.gameObject.SetActive(false);
        }
    }
}