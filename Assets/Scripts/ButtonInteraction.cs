﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonInteraction : MonoBehaviour, IPointerClickHandler
{
    GameObject highlightSprite;
    public GameObject linkedWindow;

    public GameObject errorWindow;
    public GameObject messageWindow;

    void Start()
    {
        highlightSprite = gameObject.transform.GetChild(1).gameObject;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        int clickCount = eventData.clickCount;

        if (clickCount == 1)
            OnSingleClick();
        else if (clickCount == 2)
            OnDoubleClick();
    }

    void OnSingleClick()
    {
        if (!highlightSprite.activeInHierarchy) { highlightSprite.SetActive(true); } 
        else { highlightSprite.SetActive(false); }
    }

    void OnDoubleClick()
    {
        if (gameObject.tag == "Rar")
        {
            if (GameManager.instance.GameState == 8 && messageWindow)
            {
                if (!messageWindow.activeInHierarchy) { messageWindow.SetActive(true); };
            }

            if (GameManager.instance.GameState != 8 && errorWindow)
            {
                if (!errorWindow.activeInHierarchy) { errorWindow.SetActive(true); };
            }
        } else 
        if (!linkedWindow.activeInHierarchy) { linkedWindow.SetActive(true); };

        if (highlightSprite.activeInHierarchy) { highlightSprite.SetActive(false); } 
        else { highlightSprite.SetActive(true); }

        
    }
}
//if (!linkedWindow.activeInHierarchy) { linkedWindow.SetActive(true); };