﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public Transform obstacleStart;

    public bool canSpawn;

    public Vector2 spawnDelay;
    private float spawntimer;

    public GameObject obstaclePrefab;

    private void Start()
    {
        spawntimer = Random.Range(spawnDelay.x, spawnDelay.y);
    }

    private void Update()
    {
        if (canSpawn && spawntimer >= 0)
        {
            spawntimer -= Time.deltaTime;
        }

        if (canSpawn && spawntimer <= 0)
        {
            GameObject obstacleObject = Instantiate(obstaclePrefab, 
                obstacleStart.transform.position, obstacleStart.transform.rotation);
            obstacleObject.transform.SetParent(obstacleStart);
            spawntimer = Random.Range(spawnDelay.x, spawnDelay.y);
        }
    }
}
