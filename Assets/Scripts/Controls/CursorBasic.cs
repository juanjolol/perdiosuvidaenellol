﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorBasic : MonoBehaviour
{
    public float velRate;
    Camera myCam;

    void Start()
    {
        Cursor.visible = false;
        myCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        gameObject.transform.position = Vector2.MoveTowards(transform.position, myCam.ScreenToWorldPoint(Input.mousePosition), velRate);
    }
}
