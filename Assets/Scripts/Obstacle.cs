﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public static bool canMove = true;
    public float moveSpeed;
    private void Update()
    {
        if (canMove)
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            if (transform.localPosition.x < -1.108f)
            {
                FindObjectOfType<GameRunner>().obstacles.Remove(this.gameObject);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "Player")
        {
            canMove = false;
            FindObjectOfType<GameRunner>().ResetGame();
        }
    }
}
