﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KrillAudio.Krilloud;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject blueScreen;
    private int gameState = 0;
    public int GameState
    {
        get { return gameState; }
        set
        {
            gameState = value;
            GameUpdate();
        }
    }

    public GameObject[] folders;
    public GameObject[] logs;
    public GameObject[] rars;
    public GameObject fofIcon;
    public GameObject hb;
    public GameObject congratulations;

    private KLAudioSource klSource;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;

        klSource = GetComponent<KLAudioSource>();
    }

    public void SetBlueScreen()
    {
        blueScreen.SetActive(true);
    }

    private void GameUpdate()
    {
        switch (GameState)
        {
            case 1:
                klSource.SetIntVar("selector", 0);
                klSource.Play("SFXs");
                //unlock log 2
                logs[2].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                logs[2].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 2:
                klSource.SetIntVar("selector", 0);
                klSource.Play("SFXs");
                //unlock Unity Folder
                folders[0].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                folders[0].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 3:
                klSource.SetIntVar("selector", 6);
                klSource.Play("SFXs");
                //unlock winrar1
                rars[0].SetActive(true);
                //unlock log3
                logs[3].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                logs[3].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                //lock Unity Folder
                folders[0].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
                folders[0].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
                break;
            case 4:
                klSource.SetIntVar("selector", 0);
                klSource.Play("SFXs");
                //unlock University
                folders[1].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                folders[1].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 5:
                klSource.SetIntVar("selector", 6);
                klSource.Play("SFXs");
                //unlock winrar2
                rars[1].SetActive(true);
                //lock university
                folders[1].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
                folders[1].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
                //unlock log 4
                logs[4].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                logs[4].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 6:
                klSource.SetIntVar("selector", 0);
                klSource.Play("SFXs");
                //unlock log5
                logs[5].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                logs[5].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 7:
                klSource.SetIntVar("selector", 0);
                klSource.Play("SFXs");
                //unlock log "score"
                hb.SetActive(true);
                //unlock spotify folder
                folders[2].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                folders[2].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 8:
                klSource.SetIntVar("selector", 6);
                klSource.Play("SFXs");
                //unlock winrar3
                rars[2].SetActive(true);
                //lock spotify folder
                folders[2].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
                folders[2].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
                //let player unzip rar
                break;
            case 9:
                klSource.SetIntVar("selector", 6);
                klSource.Play("SFXs");
                //disable fof icon
                fofIcon.SetActive(false);
                //disable rar files
                foreach (GameObject i in rars)
                {
                    i.SetActive(false);
                }
                //unlock log6
                logs[6].transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
                logs[6].transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                congratulations.SetActive(true);
                break;
        }
    }

    public void AdvanceStory(string fileName)
    {
        switch (fileName)
        {
            case "log1":
                if (GameState == 0)
                    GameState++;
                break;
            case "log2":
                if (GameState == 1)
                    GameState++;
                break;
            case "lab":
                if (GameState == 2)
                    GameState++;
                break;
            case "log3":
                if (GameState == 3)
                    GameState++;
                break;
            case "run":
                if (GameState == 4)
                    GameState++;
                break;
            case "log4":
                if (GameState == 5)
                    GameState++;
                break;
            case "log5":
                if (GameState == 6)
                    GameState++;
                break;
            case "piano":
                if (GameState == 7)
                    GameState++;
                break;
            case "end":
                if (GameState == 8)
                    GameState++;
                break;
        }
    }
}
