﻿using UnityEngine;

public class ShutDown : MonoBehaviour
{
    public float shutDownDelay;
    private void OnEnable()
    {
        Invoke("CloseGame", shutDownDelay);
    }

    private void CloseGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}
