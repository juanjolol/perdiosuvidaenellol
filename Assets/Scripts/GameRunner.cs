﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class GameRunner : MonoBehaviour
{
    public Rigidbody2D playerRb;
    public float forceImpulse;
    public float jumpDelay;
    private float timer;

    public TextMeshProUGUI scoreText;
    public float score;
    public Image mouseImage;

    public Transform obstacleStart;

    public bool canSpawn;

    public Vector2 spawnDelay;
    private float spawntimer;

    public GameObject obstaclePrefab;

    public Animator groundAnim;
    public Animator playerAnim;

    public GameObject clickButton;

    public List<GameObject> obstacles;

    private void Start()
    {
        timer = 0;
        spawntimer = Random.Range(spawnDelay.x, spawnDelay.y);
    }

    private void Update()
    {
        if (canSpawn && timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (canSpawn && spawntimer >= 0)
        {
            spawntimer -= Time.deltaTime;
        }

        if (canSpawn && spawntimer <= 0)
        {
            GameObject obstacleObject = Instantiate(obstaclePrefab,
                obstacleStart.transform.position, obstacleStart.transform.rotation);
            obstacleObject.transform.SetParent(obstacleStart);
            spawntimer = Random.Range(spawnDelay.x, spawnDelay.y);
            obstacles.Add(obstacleObject);
        }

    }

    public void IDK()
    {
        if (!canSpawn)
        {
            canSpawn = true;
            mouseImage.gameObject.SetActive(false);
            InvokeRepeating("AddScore", 1, 1);
        }
        else
        {
            if (timer <= 0)
            {
                playerRb.AddForce(Vector2.up * forceImpulse, ForceMode2D.Impulse);
                timer = jumpDelay;
            }
        }
    }

    public void AddScore()
    {
        if (score == 19)
        {
            GameManager.instance.AdvanceStory("run");
            transform.parent.transform.parent.gameObject.SetActive(false);
        }

        if (canSpawn)
        {
            score++;
            scoreText.text = "Score\n" + score;
        }
    }

    public void GameOver()
    {
        canSpawn = false;
        CancelInvoke();
        groundAnim.enabled = false;
        playerAnim.enabled = false;
        clickButton.SetActive(false);
    }

    public void ResetGame()
    {
        foreach (GameObject obstacle in obstacles)
        {
            Destroy(obstacle);
            score = 0;
            scoreText.text = "Score\n";
            groundAnim.enabled = true;
            playerAnim.enabled = true;
            clickButton.SetActive(true);
            Obstacle.canMove = true;
        }
    }
}
