﻿using UnityEngine;
using TMPro;

public class FoFbox : MonoBehaviour
{
    private int count = 0;
    [TextArea]
    public string[] errors;

    public TextMeshProUGUI errorText;

    private void OnDisable()
    {
        count++;
        if (count >= 5)
        {
            FindObjectOfType<GameManager>().SetBlueScreen();
        }
    }

    private void OnEnable()
    {
        errorText.text = errors[count];
    }
}
