﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KrillAudio.Krilloud;

public class TurningON : MonoBehaviour
{
    private IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<KLAudioSource>().SetIntVar("selector", 5);
        GetComponent<KLAudioSource>().Play("SFXs");

        coroutine = WaitToStart();
        StartCoroutine(coroutine);
    }

    private IEnumerator WaitToStart()
    {
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
}
