﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KrillAudio.Krilloud;

public class ErrorSoundOnEnable : MonoBehaviour
{
    void OnEnable()
    {
        GetComponent<KLAudioSource>().SetIntVar("selector", 2);
        GetComponent<KLAudioSource>().Play("SFXs");
    }
}
